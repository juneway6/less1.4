FROM debian:9 as init
RUN apt-get update && apt-get install -y wget gcc make procps libpcre3 libpcre3-dev zlib1g zlib1g-dev 
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure && make && make install

FROM debian:9
RUN apt-get update && apt-get install -y procps
WORKDIR /usr/local/nginx/sbin
COPY --from=init /usr/local/nginx/sbin/nginx .
COPY --from=init /usr/local/nginx/conf/ /usr/local/nginx/conf/
RUN mkdir ../logs && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
